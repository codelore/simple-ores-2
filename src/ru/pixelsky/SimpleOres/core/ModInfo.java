package ru.pixelsky.SimpleOres.core;

public class ModInfo 
{
	public static final String ID = "simpleores";
	public static final String NAME = "SimpleOres 2 for PixelSky";
	public static final String VERSION = "1.1.0";
}

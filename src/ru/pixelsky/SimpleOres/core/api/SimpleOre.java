package ru.pixelsky.SimpleOres.core.api;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import ru.pixelsky.SimpleOres.core.Achievements;
import ru.pixelsky.SimpleOres.core.Armor;
import ru.pixelsky.SimpleOres.core.Blocks;
import ru.pixelsky.SimpleOres.core.Items;
import ru.pixelsky.SimpleOres.core.Recipes;
import ru.pixelsky.SimpleOres.core.SimpleOres;
import ru.pixelsky.SimpleOres.core.Tools;
import ru.pixelsky.SimpleOres.core.conf.IDs;
import ru.pixelsky.SimpleOres.core.conf.Localisation;
import ru.pixelsky.SimpleOres.core.conf.Settings;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class SimpleOre extends Block
{
	private String modName;
	/**
	 * The main constructor for the blocks. 
	 * 
	 * @param par1 = ID of the block.
	 * @param material = The material of the block. This sets what tools are effective against it, the sounds it makes, etc.
	 */
	public SimpleOre(int par1, Material material, String mod) 
	{
		super(par1, material);
		this.modName = mod;
	}
	
	/**
	 * Sets the texture for the block.
	 */
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister) 
	{
		 this.blockIcon = iconRegister.registerIcon(modName + ":" + (this.getUnlocalizedName().substring(5)));
	}
    
    /**
     * Sets whether or not Silk Touch has an effect on this block. Returns true so that Onyx Ore will drop itself rather than the gem item.
     */
    protected boolean canSilkHarvest()
    {
        return true;
    }
}

package ru.pixelsky.SimpleOres.core.handlers;

import ru.pixelsky.SimpleOres.core.Achievements;
import ru.pixelsky.SimpleOres.core.Armor;
import ru.pixelsky.SimpleOres.core.Blocks;
import ru.pixelsky.SimpleOres.core.Items;
import ru.pixelsky.SimpleOres.core.Recipes;
import ru.pixelsky.SimpleOres.core.SimpleOres;
import ru.pixelsky.SimpleOres.core.Tools;
import ru.pixelsky.SimpleOres.core.conf.IDs;
import ru.pixelsky.SimpleOres.core.conf.Localisation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.ICraftingHandler;

public class HandlerCrafting implements ICraftingHandler
{
	/**
	 * Linking to the classes for easier reference.
	 */
	public static Achievements achievements;
	public static Blocks blocks;
	public static Items items;
	public static Tools tools;
	
	@Override
	public void onCrafting(EntityPlayer player, ItemStack item, IInventory craftMatrix) 
	{
		if(item.itemID == Item.pickaxeIron.itemID)
		{
			player.addStat(achievements.ironPickAch, 1);
		}
		if(item.itemID == tools.adamantiumPick.itemID)
		{
			player.addStat(achievements.adamantiumPickAch, 1);
		}
		if(item.itemID == tools.onyxPick.itemID)
		{
			player.addStat(achievements.onyxPickAch, 1);
		}
		if(item.itemID == tools.mythrilBow.itemID)
		{
			player.addStat(achievements.mythrilBowAch, 1);
		}
		if(item.itemID == tools.onyxBow.itemID)
		{
			player.addStat(achievements.onyxBowAch, 1);
		}
	}

	@Override
	public void onSmelting(EntityPlayer player, ItemStack item) 
	{
		
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.SimpleOres.core.handlers;

import net.minecraft.block.Block;

/**
 *
 * @author Smile
 */
public class XModIntegrator 
{
    public static void integrate()
    {
        try {
            Class<?> c = Class.forName("ru.pixelsky.xmod.core.XMod");
            XModData.coalBlockId = getBlockId(c, "coalBlock");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int getBlockId(Class<?> clazz, String blockVarName) throws NoSuchFieldException, IllegalAccessException {
        Block block = (Block) clazz.getField(blockVarName).get(null);
        return block.blockID;
    }
}
